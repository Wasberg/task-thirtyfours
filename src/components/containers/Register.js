import React from 'react';
import RegisterForm from '../forms/RegisterForm';

const Register = () => (
    <div>
        <h1>Register your account.</h1>
        <RegisterForm/>
    </div>
);

export default Register;