import React from 'react';
import LoginForm from '../forms/LoginForm';

const Login = () => (
    <div>
        <h1>Login to Survey Puppy.</h1>
        <LoginForm/>
    </div>
);

export default Login;