import React from 'react';

const DashboardMessage = () => (
    <div>
        <h4>Hello from your Dashboard!</h4>
    </div>
);

export default DashboardMessage;